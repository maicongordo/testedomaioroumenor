using NUnit.Framework;
using Calculadora;

namespace Calculadora.UnitTestes
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForSomar_RetornaSoma()
        {
            // arrange
            var calcular = new Calcular();

            calcular.numero1 = 1;
            calcular.numero2 = 2;
            calcular.operacoes = "+";

            // act
            var resultado = calcular.Valor();

            // assert
            Assert.That(resultado, Is.EqualTo(3));
        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForSubtrair_RetornaSubtracao()
        {
            // arrange
            var calcular = new Calcular();

            calcular.numero1 = 1;
            calcular.numero2 = 2;
            calcular.operacoes = "-";

            // act
            var resultado = calcular.Valor();

            // assert
            Assert.That(resultado, Is.EqualTo(-1));
        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForMultiplicar_RetornaMultiplicacao()
        {
            // arrange
            var calcular = new Calcular();

            calcular.numero1 = 1;
            calcular.numero2 = 2;
            calcular.operacoes = "*";

            // act
            var resultado = calcular.Valor();

            // assert
            Assert.That(resultado, Is.EqualTo(2));
        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForDividir_RetornaDivisao()
        {
            // arrange
            var calcular = new Calcular();

            calcular.numero1 = 0;
            calcular.numero2 = 2;
            calcular.operacoes = "/";

            // act
            var resultado = calcular.Valor();

            // assert
            Assert.That(resultado, Is.EqualTo(0));
        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForZero_RetornaDivisao()
        {
            // arrange
            var calcular = new Calcular();

            calcular.numero1 = 1;
            calcular.numero2 = 2;
            calcular.operacoes = "/";

            // act
            var resultado = calcular.Valor();

            // assert
            Assert.That(resultado, Is.EqualTo(0.5));
        }

       



    }
}