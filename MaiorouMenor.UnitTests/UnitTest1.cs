using NUnit.Framework;
using Minimo;

namespace MaiorouMenor.UnitTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(1, 2, 3)]
        [TestCase(1, 3, 2)]
        [TestCase(2, 3, 1)]
        [TestCase(2, 1, 3)]
        [TestCase(3, 1, 2)]
        [TestCase(3, 2, 1)]
        public void Maior_QuandoExecutadaComNumerosDiferentes_RetornaMaiorNumero(int a, int b, int c)
        {
            // arrange
            var teste = new Teste();
            teste.num1 = a;
            teste.num2 = b;
            teste.num3 = c;

            // act 
            var resultado = teste.Maior();

            //assert
            Assert.That(resultado, Is.EqualTo(3));
        }

        [TestCase(1, 1, 2, 2)]
        [TestCase(1, 2, 1, 2)]
        [TestCase(2, 1, 1, 2)]
        public void Maior_QuandoExecutadaComDoisNumeroIguais_RetornaMaiorNumero(int a, int b, int c, int esperado)
        {
            // arrange
            var teste = new Teste();
            teste.num1 = a;
            teste.num2 = b;
            teste.num3 = c;

            // act 
            var resultado = teste.Maior();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }

        [TestCase(1, 1, 1, 1)]
        public void Maior_QuandoExecutadaComTodosOsNumeroIguais_RetornaMaiorNumero(int a, int b, int c, int esperado)
        {
            // arrange
            var teste = new Teste();
            teste.num1 = a;
            teste.num2 = b;
            teste.num3 = c;

            // act 
            var resultado = teste.Maior();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }
        //FUN��O MENOR
        [TestCase(1, 2, 3)]
        [TestCase(1, 3, 2)]
        [TestCase(2, 3, 1)]
        [TestCase(2, 1, 3)]
        [TestCase(3, 1, 2)]
        [TestCase(3, 2, 1)]
        public void Menor_QuandoExecutadaComNumerosDiferentes_RetornaMenorNumero(int a, int b, int c)
        {
            // arrange
            var teste = new Teste();
            teste.num1 = a;
            teste.num2 = b;
            teste.num3 = c;

            // act 
            var resultado = teste.Menor();

            //assert
            Assert.That(resultado, Is.EqualTo(1));
        }

        [TestCase(1, 1, 2, 1)]
        [TestCase(1, 2, 1, 1)]
        [TestCase(2, 1, 1, 1)]
        public void Menor_QuandoExecutadaComDoisNumeroIguais_RetornaMenorNumero(int a, int b, int c, int esperado)
        {
            // arrange
            var teste = new Teste();
            teste.num1 = a;
            teste.num2 = b;
            teste.num3 = c;

            // act 
            var resultado = teste.Menor();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }
        [TestCase(1, 1, 1, 1)]
        public void Menor_QuandoExecutadaComTodosOsNumeroIguais_RetornaMenorNumero(int a, int b, int c, int esperado)
        {
            // arrange
            var teste = new Teste();
            teste.num1 = a;
            teste.num2 = b;
            teste.num3 = c;

            // act 
            var resultado = teste.Menor();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }

    }
}